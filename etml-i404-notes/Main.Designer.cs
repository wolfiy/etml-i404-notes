﻿namespace etml_i404_notes
{
    partial class frmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.boxName = new System.Windows.Forms.GroupBox();
            this.btnAddName = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.boxNote = new System.Windows.Forms.GroupBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.dropName = new System.Windows.Forms.ComboBox();
            this.btnAddNote = new System.Windows.Forms.Button();
            this.boxDisplay = new System.Windows.Forms.GroupBox();
            this.richNotes = new System.Windows.Forms.RichTextBox();
            this.boxName.SuspendLayout();
            this.boxNote.SuspendLayout();
            this.boxDisplay.SuspendLayout();
            this.SuspendLayout();
            // 
            // boxName
            // 
            this.boxName.Controls.Add(this.btnAddName);
            this.boxName.Controls.Add(this.txtName);
            this.boxName.Location = new System.Drawing.Point(12, 12);
            this.boxName.Name = "boxName";
            this.boxName.Size = new System.Drawing.Size(302, 60);
            this.boxName.TabIndex = 0;
            this.boxName.TabStop = false;
            this.boxName.Text = "Ajout de pseudo";
            // 
            // btnAddName
            // 
            this.btnAddName.Location = new System.Drawing.Point(221, 22);
            this.btnAddName.Name = "btnAddName";
            this.btnAddName.Size = new System.Drawing.Size(75, 23);
            this.btnAddName.TabIndex = 1;
            this.btnAddName.Text = "+";
            this.btnAddName.UseVisualStyleBackColor = true;
            this.btnAddName.Click += new System.EventHandler(this.btnAddName_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(6, 24);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(209, 20);
            this.txtName.TabIndex = 0;
            // 
            // boxNote
            // 
            this.boxNote.Controls.Add(this.txtNote);
            this.boxNote.Controls.Add(this.dropName);
            this.boxNote.Controls.Add(this.btnAddNote);
            this.boxNote.Location = new System.Drawing.Point(12, 78);
            this.boxNote.Name = "boxNote";
            this.boxNote.Size = new System.Drawing.Size(302, 85);
            this.boxNote.TabIndex = 1;
            this.boxNote.TabStop = false;
            this.boxNote.Text = "Ajout de memo";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(6, 52);
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(209, 20);
            this.txtNote.TabIndex = 5;
            // 
            // dropName
            // 
            this.dropName.FormattingEnabled = true;
            this.dropName.Location = new System.Drawing.Point(6, 23);
            this.dropName.Name = "dropName";
            this.dropName.Size = new System.Drawing.Size(209, 21);
            this.dropName.TabIndex = 4;
            this.dropName.SelectedValueChanged += new System.EventHandler(this.dropName_SelectedValueChanged);
            // 
            // btnAddNote
            // 
            this.btnAddNote.Location = new System.Drawing.Point(221, 23);
            this.btnAddNote.Name = "btnAddNote";
            this.btnAddNote.Size = new System.Drawing.Size(75, 23);
            this.btnAddNote.TabIndex = 2;
            this.btnAddNote.Text = "+";
            this.btnAddNote.UseVisualStyleBackColor = true;
            this.btnAddNote.Click += new System.EventHandler(this.btnAddNote_Click);
            // 
            // boxDisplay
            // 
            this.boxDisplay.Controls.Add(this.richNotes);
            this.boxDisplay.Location = new System.Drawing.Point(12, 169);
            this.boxDisplay.Name = "boxDisplay";
            this.boxDisplay.Size = new System.Drawing.Size(296, 209);
            this.boxDisplay.TabIndex = 2;
            this.boxDisplay.TabStop = false;
            this.boxDisplay.Text = "Affichage";
            // 
            // richNotes
            // 
            this.richNotes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.richNotes.Location = new System.Drawing.Point(7, 22);
            this.richNotes.Name = "richNotes";
            this.richNotes.Size = new System.Drawing.Size(283, 172);
            this.richNotes.TabIndex = 0;
            this.richNotes.Text = "";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 393);
            this.Controls.Add(this.boxDisplay);
            this.Controls.Add(this.boxNote);
            this.Controls.Add(this.boxName);
            this.Name = "frmMain";
            this.Text = "Exercice 9";
            this.boxName.ResumeLayout(false);
            this.boxName.PerformLayout();
            this.boxNote.ResumeLayout(false);
            this.boxNote.PerformLayout();
            this.boxDisplay.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox boxName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.GroupBox boxNote;
        private System.Windows.Forms.GroupBox boxDisplay;
        private System.Windows.Forms.Button btnAddName;
        private System.Windows.Forms.Button btnAddNote;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.ComboBox dropName;
        private System.Windows.Forms.RichTextBox richNotes;
    }
}

