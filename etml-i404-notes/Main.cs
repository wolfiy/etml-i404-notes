﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace etml_i404_notes
{
    public partial class frmMain : Form
    {
        #region Constantes
        /// <summary>
        /// Nombre maximal de pseudos.
        /// </summary>
        private const int MAX_USERNAMES = 5;

        /// <summary>
        /// Nombre de memos maximal.
        /// </summary>
        private const int MAX_NOTES = 10;
        #endregion

        #region Variables
        /// <summary>
        /// Dictionnaire contenant une liste pour chaque utilisateur.
        /// </summary>
        private Dictionary<string, ArrayList> _notes;
        #endregion

        /// <summary>
        /// Constructeur de la fenêtre principale.
        /// </summary>
        public frmMain()
        {
            InitializeComponent();
            _notes = new Dictionary<string, ArrayList>();
        } // frmMain()

        #region Logique
        /// <summary>
        /// Permet d'ajouter un pseudo à la liste s'il est valide.
        /// </summary>
        /// <param name="usr">le pseudo à ajouter.</param>
        private void AddUsername(string usr)
        {
            if (usr == null || usr == "")
            {
                MessageBox.Show("Veuillez entrer un pseudo!", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (_notes.Count >= MAX_USERNAMES)
            {
                MessageBox.Show("Nombre de pseudo maximal atteint!", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else if (_notes.ContainsKey(usr))
            {
                MessageBox.Show("Pseudo déjà existant!", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                _notes.Add(usr, new ArrayList());
                txtName.Clear();
                dropName.Items.Add(usr);
                dropName.Text = usr;
            }
        } // AddUsername()

        /// <summary>
        /// Permet d'ajouter un memo et de le lier à un utilisateur.
        /// </summary>
        /// <param name="usr">l'auteur du memo.</param>
        private void AddNote(string usr)
        {
            var list = _notes[usr];
            var text = txtNote.Text;

            if (list.Count < MAX_NOTES
                && txtNote.Text != ""
                && !list.Contains(text))
            {
                list.Add(text);
            }
        } // AddNote()

        /// <summary>
        /// Permet d'afficher les notes d'un utilisateur.
        /// </summary>
        /// <param name="usr">le nom de l'utilisateur.</param>
        private void ShowUserNotes(string usr)
        {
            string allNotes = "";
            var list = _notes[usr];

            foreach (string note in list)
                allNotes += note + "\n";

            richNotes.Text = allNotes;
        } // ShowUserNotes()
        #endregion

        #region Evénements
        private void btnAddName_Click(object sender, EventArgs e)
        {
            AddUsername(txtName.Text);
        }

        private void btnAddNote_Click(object sender, EventArgs e)
        {
            AddNote(dropName.Text);
            ShowUserNotes(dropName.Text);
        }

        private void dropName_SelectedValueChanged(object sender, EventArgs e)
        {
            ShowUserNotes(dropName.Text);
        }
        #endregion
    }
}
